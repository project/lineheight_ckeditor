Ckeditor line height plugin
## INTRODUCTION

Adds the functionality to set the line-height property of an element similar to the font-size to CKEditor.

Usage:
Go to ckeditor config and add this to your panel

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/lineheight_ckeditor

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/lineheight_ckeditor


## REQUIREMENTS

No special requirements.


## INSTALLATION

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:
* Yusef Mohamadi (yuseferi) - https://www.drupal.org/u/yuseferi

