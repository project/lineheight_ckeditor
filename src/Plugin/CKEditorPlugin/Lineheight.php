<?php

namespace Drupal\lineheight_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Line Height" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lineheight",
 *   label = @Translation("CKEditor Line Height"),
 *   module = "lineheight_ckeditor"
 * )
 */
class Lineheight extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'lineheight_ckeditor') . '/plugin/plugin.js';

  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'lineheight' => [
        'label' => $this->t('Line height'),
        'image' => drupal_get_path('module', 'lineheight_ckeditor') . '/icons/lineheight.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'line_height' => "0.5;0.6;0.7;0.8;0.9;1;1.1;1.2;1.3;1.4;1.5;1.6;1.7;1.8;1.9;2;2.2;2.5;3;3.5;"
    ];
}}
